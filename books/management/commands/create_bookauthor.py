from books.models import BookAuthor, Book
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
import random
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Create random users'

    def handle(self, *args, **kwargs):
        books = Book.objects.all()
        authors = User.objects.all()
        for book in books:
            for i in range(0, 3):
                author = random.choice(authors)
                # print(author)
                # book_author = BookAuthor()
                # book_author.book = book
                # book_author.user = author
                BookAuthor.objects.create(book=book, user=author)
