from books.models import BookCategory, Book, Category
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
import random


class Command(BaseCommand):
    help = 'Create random users'

    def handle(self, *args, **kwargs):
        books = Book.objects.all()
        categories = Category.objects.all()
        for book in books:
            for i in range(0, 3):
                # book_category = BookCategory()
                category = random.choice(categories)
                # book_category.book = book
                # book_category.category = category
                BookCategory.objects.create(book=book, category=category)
