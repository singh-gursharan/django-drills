from books.models import Book
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
import random


class Command(BaseCommand):
    help = 'Create random users'

    def add_arguments(self, parser):
        parser.add_argument('total', type=int,
                            help='Indicates the number of users to be created')

    def handle(self, *args, **kwargs):
        total = kwargs['total']
        count = Book.objects.count()
        for i in range(count+1, count+total):
            b = Book.objects.create(
                title='b'+str(i), price=random.randrange(10, 100, 1))
