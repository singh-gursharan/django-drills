from books.models import Cart, CartItem, Book
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
import random
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Create random users'

    def handle(self, *args, **kwargs):
        carts = Cart.objects.all()
        books = Book.objects.all()
        for cart in carts:
            for i in range(0, 3):
                book = random.choice(books)
                # cart_item = CartItem()
                # cart_item.cart = cart
                # cart_item.book = book
                CartItem.objects.create(cart=cart, book=book)
