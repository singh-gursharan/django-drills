from books.models import Cart
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
import random
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Create random users'

    def add_arguments(self, parser):
        parser.add_argument('total', type=int,
                            help='Indicates the number of users to be created')

    def handle(self, *args, **kwargs):
        total = kwargs['total']
        count = Cart.objects.count()
        users = User.objects.all()
        for i in range(count+1, count+total+1):
            user = random.choice(users)
            c = Cart.objects.create(
                user=user)
            # c.save()
