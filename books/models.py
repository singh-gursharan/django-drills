from django.db import models
from django.conf import settings

# Create your models here.


class Book(models.Model):
    title = models.CharField(max_length=30)
    price = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class Category(models.Model):
    name = models.CharField(max_length=30)


class BookCategory(models.Model):
    book = models.ForeignKey(
        'Book', on_delete=models.CASCADE, related_name="categores")
    category = models.ForeignKey(
        'Category', on_delete=models.CASCADE, related_name="books")


class BookAuthor(models.Model):
    book = models.ForeignKey(
        'Book', on_delete=models.CASCADE, related_name="authors")
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, related_name="books")


class Cart(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, related_name="carts")


class CartItem(models.Model):
    cart = models.ForeignKey('Cart',
                             on_delete=models.CASCADE, related_name="items")
    book = models.ForeignKey(
        'Book', on_delete=models.CASCADE, related_name="carts")
    quantity = models.IntegerField(default=1)
    
