from books.models import Book, Category, BookAuthor, BookCategory, Cart, CartItem
import logging
from django.contrib.auth.models import User
from django.db.models import Max, Count, Sum
l = logging.getLogger('django.db.backends')
l.setLevel(logging.DEBUG)
l.addHandler(logging.StreamHandler())


def fetch_book(id):
    book = Book.objects.get(id=id)
    return book


def fetch_books(id1, id2, id3):
    books = Book.objects.filter(id__in=[id1, id2, id2])
    return books


def fetch_book_by_category(category_name):
    books = Category.objects.filter(name=category_name)[0].books
    return books


def books_count_by_category(category_name):
    books_count = Category.objects.filter(name=category_name)[0].books.count()
    return books_count


def most_books_author():
    data = BookAuthor.objects.values('user__username')\
        .annotate(book_count=Count('book'))\
        .order_by('-book_count')[0]
    return data['user__username']


def books_total_cost():
    cost = Book.objects.all().aggregate(Sum('price'))
    return cost['price__sum']


def most_expensive_book():
    book = Book.objects.all().aggregate(Max('price'))
    return book['price__max']


def book_with_price_more_than(price):
    book = Book.objects.filter(price__gte=price).order_by('-price')
    return book


def delete_book(id):
    Book.objects.get(id=id).delete()


def cart_items_and_books(id):
    obj = CartItem.objects.select_related('book').filter(cart_id=id)
    return obj
